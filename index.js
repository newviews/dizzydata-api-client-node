var deprecate = depd('dizzydata-api-client');

deprecate('This module was replaced by the "dizzydata" module');

module.exports = require('dizzydata');